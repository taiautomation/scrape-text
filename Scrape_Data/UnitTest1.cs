﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Mime;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Interactions;
using System.Threading;
using Excel = Microsoft.Office.Interop.Excel;

namespace ScrapeData
{
    [TestClass]

    public class ScrapeData
    {
        private TestContext testContextInstance;
        private IWebDriver _driver;
        private int _check;
        private string _targetURL;
        private string _subURL;
        private string _name;
        private string _address;
        private string _urn;
        private string _type;
        private string _latestReport;
        private string _rating;
        private string _ageRange;
        private string _gender;
        private string _numberOfPupils;
        private string _capacity;
        private string _principal;
        private string _boardingProvision;
        private string _religious;
        private string _localAuthority;
        private string _region;
        private string _telephone;
        private string _website;
        private string _registeredBy;
        private string _samePostcode;
        private string _sameRegistered;
        private string _tempString;
        private string _compareString;
        private int _totalRegisteredDiv;
        private List<IWebElement> _totalRegistered;
        private List<IWebElement> _totalSameRegistered;
        private List<IWebElement> _totalSamePostcode;
        private List<IWebElement> _totalResult;

        private void GetData()
        {
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            _totalResult = new List<IWebElement>(_driver.FindElements(By.ClassName("search-result")));
            for (var i = 1; i <= _totalResult.Capacity; i++)
            {
                _registeredBy = "";
                _sameRegistered = "";
                _samePostcode = "";
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/h3/a")));
                _name = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/h3/a"))).Text;

                _check = _driver.FindElements(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/address")).Count;
                if (_check == 1)
                {
                    try
                    {
                        _address = _driver.FindElement(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/address")).Text;
                    }
                    catch (WebDriverTimeoutException)
                    {
                        _address = "";
                    }
                }

                _check = _driver.FindElements(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/div/p/strong")).Count;
                _rating = _check == 1 ? _driver.FindElement(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/div/p/strong")).Text : "";

                _check = _driver.FindElements(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/ul/li[1]")).Count;
                _urn = _check == 1 ? "'" + _driver.FindElement(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/ul/li[1]")).Text.Substring(4) : "";

                _check = _driver.FindElements(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/ul/li[2]")).Count;
                _type = _check == 1 ? _driver.FindElement(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/ul/li[2]")).Text.Substring(6) : "";

                _check = _driver.FindElements(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/ul/li[3]")).Count;
                _latestReport = _check == 1 ? _driver.FindElement(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/ul/li[3]")).Text.Substring(15) : "";

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='main']/div[2]/div/div[3]/ul/li[" + i + "]/h3/a"))).Click();
                _subURL = _driver.Url;

                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.CssSelector(".timeline__title.heading__main")));

                _check = _driver.FindElements(By.XPath("//div[@class='module info-block info-block--pupils']")).Count;
                if (_check == 0)
                {
                    _ageRange = "";
                    _gender = "";
                    _numberOfPupils = "";
                    _capacity = "";
                }
                else
                {
                    _check = _driver.FindElements(By.XPath("//span[./text()='Age Range']/following-sibling::span")).Count;
                    _ageRange = _check == 1 ? "'" + _driver.FindElement(By.XPath("//span[./text()='Age Range']/following-sibling::span")).Text : "";

                    _check = _driver.FindElements(By.XPath("//span[./text()='Gender']/following-sibling::span")).Count;
                    _gender = _check == 1 ? _driver.FindElement(By.XPath("//span[./text()='Gender']/following-sibling::span")).Text : "";

                    _check = _driver.FindElements(By.XPath("//span[./text()='Number of pupils']/following-sibling::span")).Count;
                    _numberOfPupils = _check == 1 ? _driver.FindElement(By.XPath("//span[./text()='Number of pupils']/following-sibling::span")).Text : "";

                    _check = _driver.FindElements(By.XPath("//span[./text()='School capacity']/following-sibling::span")).Count;
                    _capacity = _check == 1 ? _driver.FindElement(By.XPath("//span[./text()='School capacity']/following-sibling::span")).Text : "";
                }

                _check = _driver.FindElements(By.XPath("//span[./text()='Principal']/following-sibling::span")).Count;
                _principal = _check == 1 ? _driver.FindElement(By.XPath("//span[./text()='Principal']/following-sibling::span")).Text : "";

                _check = _driver.FindElements(By.XPath("//span[./text()='Boarding provision']/following-sibling::span")).Count;
                _boardingProvision = _check == 1 ? _driver.FindElement(By.XPath("//span[./text()='Boarding provision']/following-sibling::span")).Text : "";

                _check = _driver.FindElements(By.XPath("//span[./text()='Type']/following-sibling::span")).Count;
                _type = _check == 1 ? _driver.FindElement(By.XPath("//span[./text()='Type']/following-sibling::span")).Text : "";

                _check = _driver.FindElements(By.XPath("//span[./text()='Religious character']/following-sibling::span")).Count;
                _religious = _check == 1 ? _driver.FindElement(By.XPath("//span[./text()='Religious character']/following-sibling::span")).Text : "";

                _check = _driver.FindElements(By.XPath("//span[./text()='Website']/following-sibling::span")).Count;
                _website = _check == 1 ? _driver.FindElement(By.XPath("//span[./text()='Website']/following-sibling::span")).Text : "";

                _check = _driver.FindElements(By.XPath("//div[@class='module info-block info-block--registered-by']")).Count;
                if (_check == 1)
                {
                    _totalRegisteredDiv = _driver.FindElements(By.XPath("//div[@class='module info-block info-block--registered-by']/h3")).Count;
                    if (_totalRegisteredDiv == 1)
                    {
                        _totalRegistered = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='main']/div[2]/div/div/div[2]/div[3]/ul[1]/li/a")));
                        for (var j = 1; j <= _totalRegistered.Capacity; j++)
                        {
                            _tempString = _driver.FindElement(By.XPath("//*[@id='main']/div[2]/div/div/div[2]/div[3]/ul[1]/li/a")).Text;
                            _registeredBy = _registeredBy + _tempString + "\n";
                        }
                    }
                    else if (_totalRegisteredDiv == 2)
                    {
                        _totalRegistered = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='main']/div[2]/div/div/div[2]/div[3]/ul[1]/li/a")));
                        for (var j = 1; j <= _totalRegistered.Capacity; j++)
                        {
                            _tempString = _driver.FindElement(By.XPath("//*[@id='main']/div[2]/div/div/div[2]/div[3]/ul[1]/li[" + j + "]/a")).Text;
                            _registeredBy = _registeredBy + _tempString + "\n";
                        }

                        _totalSameRegistered = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='main']/div[2]/div/div/div[2]/div[3]/ul[2]/li/a")));
                        for (var j = 1; j <= _totalSameRegistered.Capacity; j++)
                        {
                            _tempString = _driver.FindElement(By.XPath("//*[@id='main']/div[2]/div/div/div[2]/div[3]/ul[2]/li[" + j + "]/a")).Text;
                            _sameRegistered = _sameRegistered + _tempString + "\n";
                        }
                    }
                    else
                    {
                        _registeredBy = "";
                        _sameRegistered = "";
                    }
                }

                _check = _driver.FindElements(By.XPath("//div[@class='module info-block info-block--postcode-links']")).Count;
                if (_check == 1)
                {
                    _totalSamePostcode = new List<IWebElement>(_driver.FindElements(By.XPath("//*[@id='main']/div[2]/div/div/div[2]/div[4]/ul/li")));
                    for (var j = 1; j <= _totalSamePostcode.Capacity; j++)
                    {
                        _tempString = _driver.FindElement(By.XPath("//*[@id='main']/div[2]/div/div/div[2]/div[4]/ul/li[" + j + "]/a")).Text;
                        _samePostcode = _samePostcode + _tempString + "\n";
                    }
                }
                else
                {
                    _samePostcode = "";
                }

                _check = _driver.FindElements(By.XPath("//span[./text()='Local authority']/following-sibling::span")).Count;
                _localAuthority = _check == 1 ? _driver.FindElement(By.XPath("//span[./text()='Local authority']/following-sibling::span")).Text : "";

                _check = _driver.FindElements(By.XPath("//span[./text()='Region']/following-sibling::span")).Count;
                _region = _check == 1 ? _driver.FindElement(By.XPath("//span[./text()='Region']/following-sibling::span")).Text : "";

                _check = _driver.FindElements(By.XPath("//span[./text()='Telephone']/following-sibling::span")).Count;
                _telephone = _check == 1 ? "'" + _driver.FindElement(By.XPath("//span[./text()='Telephone']/following-sibling::span")).Text : "";
                
                _driver.Navigate().Back();
                AddTextToExcel();
            }
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='main']/div[2]/div/div[3]/div[3]/div[2]/a"))).Click();
        }

        private void AddTextToExcel()
        {
            Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;
            object misValue = System.Reflection.Missing.Value;
            xlApp.DisplayAlerts = false;
            //xlApp.Visible = true;

            var filePath = new FileInfo("D:\\getData.xls");

            if (filePath.Exists)
            {
                xlWorkBook = xlApp.Workbooks.Open("D:\\getData.xls");
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.Item[1];

                int lastUsedRow = xlWorkSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing).Row;
                if (lastUsedRow == 200)
                {
                    _driver.Quit();
                }

                xlWorkSheet.Cells[lastUsedRow + 1, 1] = _subURL;
                xlWorkSheet.Cells[lastUsedRow + 1, 2] = _urn;
                xlWorkSheet.Cells[lastUsedRow + 1, 3] = _latestReport;
                xlWorkSheet.Cells[lastUsedRow + 1, 4] = _rating;
                xlWorkSheet.Cells[lastUsedRow + 1, 5] = _name;
                xlWorkSheet.Cells[lastUsedRow + 1, 6] = _address;
                xlWorkSheet.Cells[lastUsedRow + 1, 7] = _ageRange;
                xlWorkSheet.Cells[lastUsedRow + 1, 8] = _gender;
                xlWorkSheet.Cells[lastUsedRow + 1, 9] = _numberOfPupils;
                xlWorkSheet.Cells[lastUsedRow + 1, 10] = _capacity;
                xlWorkSheet.Cells[lastUsedRow + 1, 11] = _principal;
                xlWorkSheet.Cells[lastUsedRow + 1, 12] = _boardingProvision;
                xlWorkSheet.Cells[lastUsedRow + 1, 13] = _type;
                xlWorkSheet.Cells[lastUsedRow + 1, 14] = _religious;
                xlWorkSheet.Cells[lastUsedRow + 1, 15] = _localAuthority;
                xlWorkSheet.Cells[lastUsedRow + 1, 16] = _region;
                xlWorkSheet.Cells[lastUsedRow + 1, 17] = _telephone;
                xlWorkSheet.Cells[lastUsedRow + 1, 18] = _website;
                xlWorkSheet.Cells[lastUsedRow + 1, 19] = _registeredBy;
                xlWorkSheet.Cells[lastUsedRow + 1, 20] = _samePostcode;
                xlWorkSheet.Cells[lastUsedRow + 1, 21] = _sameRegistered;

                xlWorkBook.Save();
                xlWorkBook.Close(true, "D:\\getData.xls", misValue);
            }

            else
            {
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.Item[1];
                range = xlWorkSheet.get_Range("A1", "U1");
                range.Font.Bold = true;

                xlWorkSheet.Cells[1, 1] = "URL";
                xlWorkSheet.Cells[1, 2] = "URN";
                xlWorkSheet.Cells[1, 3] = "Latest report";
                xlWorkSheet.Cells[1, 4] = "Rating";
                xlWorkSheet.Cells[1, 5] = "Name";
                xlWorkSheet.Cells[1, 6] = "Address";
                xlWorkSheet.Cells[1, 7] = "Age Range";
                xlWorkSheet.Cells[1, 8] = "Gender";
                xlWorkSheet.Cells[1, 9] = "Number of pupils";
                xlWorkSheet.Cells[1, 10] = "School capacity";
                xlWorkSheet.Cells[1, 11] = "Principal";
                xlWorkSheet.Cells[1, 12] = "Boarding Provision";
                xlWorkSheet.Cells[1, 13] = "Type";
                xlWorkSheet.Cells[1, 14] = "Religious Character";
                xlWorkSheet.Cells[1, 15] = "Local authority";
                xlWorkSheet.Cells[1, 16] = "Region";
                xlWorkSheet.Cells[1, 17] = "Telephone";
                xlWorkSheet.Cells[1, 18] = "Website";
                xlWorkSheet.Cells[1, 19] = "Registered by";
                xlWorkSheet.Cells[1, 20] = "At the same Postcode";
                xlWorkSheet.Cells[1, 21] = "Under the same Registered Person";

                xlWorkSheet.Cells[2, 1] = _subURL;
                xlWorkSheet.Cells[2, 2] = _urn;
                xlWorkSheet.Cells[2, 3] = _latestReport;
                xlWorkSheet.Cells[2, 4] = _rating;
                xlWorkSheet.Cells[2, 5] = _name;
                xlWorkSheet.Cells[2, 6] = _address;
                xlWorkSheet.Cells[2, 7] = _ageRange;
                xlWorkSheet.Cells[2, 8] = _gender;
                xlWorkSheet.Cells[2, 9] = _numberOfPupils;
                xlWorkSheet.Cells[2, 10] = _capacity;
                xlWorkSheet.Cells[2, 11] = _principal;
                xlWorkSheet.Cells[2, 12] = _boardingProvision;
                xlWorkSheet.Cells[2, 13] = _type;
                xlWorkSheet.Cells[2, 14] = _religious;
                xlWorkSheet.Cells[2, 15] = _localAuthority;
                xlWorkSheet.Cells[2, 16] = _region;
                xlWorkSheet.Cells[2, 17] = _telephone;
                xlWorkSheet.Cells[2, 18] = _website;
                xlWorkSheet.Cells[2, 19] = _registeredBy;
                xlWorkSheet.Cells[2, 20] = _samePostcode;
                xlWorkSheet.Cells[2, 21] = _sameRegistered;

                xlWorkBook.SaveAs("D:\\getData.xls", Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue,
                    misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
            }
            
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
        }

        [TestMethod]
        [TestCategory("Chrome")]

        public void _Scrape_Data()
        {
            //Go get data
            _driver.Navigate().GoToUrl(_targetURL);
            //_driver.Manage().Window.Maximize();
            bool stillExist = _driver.FindElement(By.XPath("//*[@id='main']/div[2]/div/div[3]/div[3]/div[2]/a")).Displayed;
            while ( stillExist == true )
            {
                GetData();
            }
        }

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void SetupTest()
        {
            _targetURL = "https://reports.ofsted.gov.uk/search?q=&location=&lat=&lon=&radius=&level_1_types=2&level_2_types%5B0%5D=6&level_2_types%5B1%5D=7&start=160&status%5B0%5D=1&rows=10";

            //Open Chrome in Incognito mode
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--incognito");
            //_driver = new ChromeDriver(@"D:\AutoKaercher\AutoKaercher\bin\Debug", options);
            _driver = new ChromeDriver(options);

            _driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);

        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            _driver.Quit();
        }
    }
}